.. SecKit SA IDM Common documentation master file, created by

.. _using:

================================================
Using Windows Assets and Identities
================================================

Before continuing with this section ensure you have completed the quickstart tutorial.

Enrichment Lookups
-------------------------------------------

.. _location:

seckit_idm_windows_identities_accounts_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the sAMAccountName field of the ActiveDirectory sync event to enrich the record. This is often used to identify privileged and service accounts.

account
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

account_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

account_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

account_watchlist
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Include in watchlist this should only be applied to accounts which should not be seen in events.

seckit_idm_windows_identities_bunit_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the calculated bunit field of the ActiveDirectory sync event to enrich the record. This is often used to identify users with access to sensitive data. See macro for bunit calculation.

bunit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

bunit_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

bunit_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

seckit_idm_windows_identities_members_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the sAMAccountName field of the ActiveDirectory sync event to enrich the record. This is often used to identify privileged and service accounts.


account
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

account_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

account_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

account_watchlist
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Include in watchlist this should only be applied to accounts which should not be seen in events.

seckit_idm_windows_identities_bunit_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the memberOf field of the ActiveDirectory sync event to enrich the record. This is often used to identify users with access to sensitive data or privileged.

memberOf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

member_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

member_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

member_watchlist
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized


seckit_idm_windows_identities_nha_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the sAMAccountName field of the ActiveDirectory sync event to enrich the record. This is often used to identify privileged and service accounts. This list works exactly as the account lookup. It is provided as a separate lookup to help change management where the contents are subject to audit review.

identity
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

nha_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

nha_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

nha_watchlist
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Include in watchlist this should only be applied to accounts which should not be seen in events.


seckit_idm_windows_identities_org_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the organizational unit to enrich identities

org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

org_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

org_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

seckit_idm_windows_identities_title_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the person record's job title to enrich identities. Typically this is utilized to identify important persons and persons which may have access to sensitive information such as attorneys and execs.

title
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

title_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

title_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

title_watchlist
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Include in watchlist this should only be applied to accounts which should not be seen in events.

seckit_idm_windows_assets_bunit_lookup
+++++++++++++++++++++++++++++++++++++++++++++

Utilizes the calculated bunit field of the ActiveDirectory sync event to enrich the record. This is often used to usage of assets. See Macro ....

bunit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

bunit_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

bunit_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

bunit_expected
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Identify this asset as expected for ES

seckit_idm_windows_os_lookup
++++++++++++++++++++++++++++

The lookup enriches the asset object with operating system information and support status.
Changes to this lookup should be submitted via issue or PR in the project repository.

seckit_idm_windows_os_win_interfaces_lookup
+++++++++++++++++++++++++++++++++++++++++++

The lookup enriches optional data to detect the ip of assets where the interface is identified as dhcp assigned however the ip is effectively static as it is assigned by a virtualization management solution. Changes to this lookup should be submitted via issue or PR in the project repository.

seckit_idm_windows_pf_roles_lookup
++++++++++++++++++++++++++++++++++

This lookip enriches the category of assets based on the installed roles as collected by Splunk_TA_windows.

role_name
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

role_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

role_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

role_expected
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Identify this asset as expected for ES

role_pci_domain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Frequently used to define systems whose roles such as Active Directory services Mandate inclusion in the trust domain for PCI.


seckit_idm_windows_assets_org_lookup
++++++++++++++++++++++++++++++++++++

This lookup utilizes the organizational unit of the asset.

org
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Case insensitive and wild card match.

org_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional categories to apply

org_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Priority to apply note the highest priority is utilized

org_expected
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Identify this asset as expected for ES

Apply the updated configuration to your assets
----------------------------------------------

*Update the configuration files using Enterprise Security Content Management*

- As a es_admin login to Splunk Enterprise Security
- Navigate to the configure menu
- Select Content Management
- Select "SecKit SA IDM Common" from the app menu
- Find "SecKit IDM Common network location" by name and click update file upload the file created above ``seckit_idm_pre_cidr_location.csv``
- Find "SecKit IDM Common network categories" by name and click update file upload the file created above ``seckit_idm_pre_cidr_category.csv``

*Force Merge of Assets*

The following process can be used at any time to force immediate updates of asset files

- Navigate to a Splunk Search window
- Run the search:

::

| savedsearch "seckit_idm_common_assets_networks_lookup_gen"

- Run the search:

::

| from savedsearch:"Identity - Asset String Matches - Lookup Gen"

- Run the search:

::

| from savedsearch:"Identity - Identity Matches - Lookup Gen"

*Verification*

- As an ES user (or above) navigate to Enterprise security
- Select Security Domains from the menu
- Select Identity from the drop down
- Select Asset Center
- View the record as defined above if additional records are displayed from other sources sort/scroll to locate

Scheduled Searches and Enabled Input Tasks
-------------------------------------------

Inputs
++++++++++++++++++++++++++++++++++++++++++++++++++++

identity_manager://seckit_idm_common_assets_networks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Utilized to enable the usage of the main combined lookup by Enterprise Security Identity Manager

Scheduled Searches
++++++++++++++++++++++++++++++++++++++++++++++++++++

seckit_idm_common_assets_networks_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Produces the lookup ``seckit_idm_common_assets_networks_lookup`` used as input in ``identity_manager://seckit_idm_common_assets_networks``. The default schedule will produce a new lookup every 4 hours.


seckit_idm_combined_cidr_category_by_str_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Combines the csv lookup ``seckit_idm_pre_cidr_category_by_str_lookup`` and search managed collection ``seckit_idm_common_event_cidr_category`` to produced the lookup ``seckit_idm_combined_cidr_category_by_str_lookup``. This lookup is utilized by the saved search ``seckit_idm_common_assets_networks_lookup_gen`` to produce the network assets file. The default schedule will produce a new file every 30 min.

seckit_idm_common_event_cidr_category_from_dm_network_session_dhcp
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Utilizes the network session data model to identify network segments managed using DHCP to automatically categorize subnets. The default schedule will detect new subnets every 4 hours.


seckit_idm_common_event_cidr_category_age
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ages entries in the lookup ``seckit_idm_common_event_cidr_category_age`` where last is non zero and not updated in the prior year. The default schedule will trim the lookup once per day


seckit_idm_common_assets_expected_tracker_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Updates the lookup ``seckit_idm_common_assets_host_expected_tracker_lookup`` based on universal forwarder internal logs to identify hosts which should be set as is_expected. The default schedule search runs at the top of the hour using only the last 15m of prior data.

seckit_idm_common_assets_expected_tracker_age
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ages entries in the lookup ``seckit_idm_common_assets_host_expected_tracker_lookup`` not updated in the prior year. The default schedule will trim the lookup once per day.

seckit_idm_pre_cidr_category_by_str_lookup_ftr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ensures the lookup ``seckit_idm_pre_cidr_category_by_str_lookup`` exists and contains the correct fields. The default schedule of the search uses a special configuration option run_on_startup and run_n_times to ensure the search runs on only once.

seckit_idm_common_assets_networks_lookup_ftr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ensures the lookup ``seckit_idm_common_assets_networks_lookup`` exists and contains the correct fields. The default schedule of the search uses a special configuration option run_on_startup and run_n_times to ensure the search runs on only once.

seckit_idm_pre_host_static_lookup_ftr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ensures the lookup ``seckit_idm_pre_host_static_lookup`` exists and contains the correct fields. The default schedule of the search uses a special configuration option run_on_startup and run_n_times to ensure the search runs on only once.

Scheduled Searches and Enabled Input Tasks
-------------------------------------------

Inputs
++++++++++++++++++++++++++++++++++++++++++++++++++++

identity_manager://seckit_idm_windows_assets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Utilized to enable the usage of the assets discovered from ActiveDirectory lookup by Enterprise Security Identity Manager


identity_manager://seckit_idm_windows_assets_identities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Utilized to enable the usage of the identities discovered from ActiveDirectory for computer accounts lookup by Enterprise Security Identity Manager

identity_manager://seckit_idm_windows_identities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Utilized to enable the usage of the identities discovered from ActiveDirectory lookup by Enterprise Security Identity Manager.

Scheduled Searches
++++++++++++++++++++++++++++++++++++++++++++++++++++

seckit_idm_windows_assets_identities_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Produces the lookup ``seckit_idm_windows_assets_identities_lookup`` used as input in ``identity_manager://seckit_idm_windows_assets_identities``. The default schedule will produce a new lookup every 4 hours.

seckit_idm_windows_assets_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Produces the lookup ``seckit_idm_windows_assets_lookup`` used as input in ``identity_manager://seckit_idm_windows_assets``. The default schedule will produce a new lookup every 4 hours.

seckit_idm_windows_identities_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Produces the lookup ``seckit_idm_windows_identities_lookup`` used as input in ``identity_manager://seckit_idm_windows_identities``. The default schedule will produce a new lookup every 4 hours.

seckit_idm_windows_pf_roles_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manages the collection lookup ``seckit_idm_windows_asset_os_role_lookup`` The default schedule will update the collection hourly.

seckit_idm_windows_activedirectory_computers_load
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manually executed to load the collection using historical events or reload collection (unusual). This is not scheduled and should not be under normal conditions.

seckit_idm_windows_activedirectory_computers_tracker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manages the collection lookup ``seckit_idm_windows_activedirectory_computers_lookup`` The default schedule will update every 5 minutes using the prior five minutes.

seckit_idm_windows_activedirectory_people_load
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manually executed to load the collection using historical events or reload collection (unusual). This is not scheduled and should not be under normal conditions.

seckit_idm_windows_activedirectory_persons_tracker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manages the collection lookup ``seckit_idm_windows_activedirectory_persons_lookup`` The default schedule will update every 5 minutes using the prior five minutes.

seckit_idm_windows_asset_interface_load
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manually executed to load the collection using historical events or reload collection (unusual). This is not scheduled and should not be under normal conditions.

seckit_idm_windows_asset_interface_tracker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manages the collection lookup ``seckit_idm_windows_asset_interface_lookup`` The default schedule will update every 5 minutes using the prior five minutes.

seckit_idm_windows_asset_os_role_load
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manually executed to load the collection using historical events or reload collection (unusual). This is not scheduled and should not be under normal conditions.

seckit_idm_windows_asset_os_role_tracker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Manages the collection lookup ``seckit_idm_windows_asset_os_role_lookup`` The default schedule will update every 5 minutes using the prior five minutes.
