.. SecKit SA IDM Windows documentation master file, created by

.. _install:

================================================
Installation
================================================

Installation of the apps is intended to be minimally impactful to a Splunk ES environment.
If existing assets and identities have been configured care should be take to ensure an asset
or identity is only defined once.

Migration from legacy assets and identities
-------------------------------------------

Enterprise Security does not "merge" records from multiple sources having multiple
conflicting definitions can impact systems active users.

- Identify and remove the identity file definition from Splunk Enterprise Security
- Identify and remove the lookup definition from Splunk Enterprise
- Identify and remove the lookup file from disk. *Important to ensure large bundles do not impact search replication*

Installation
-------------------------------------------

This add on is installed on the Splunk Enterprise Security Search head.

Splunk Enterprise:

- First Install SecKit IDM Common `<https://splunkbase.splunk.com/app/3055/>`__
- Download the latest published release of `SecKit Windows Assets Add-on for Splunk Enterprise Security <https://splunkbase.splunk.com/app/3059/>`_.
- Download the latest master build of SecKit SA IDM Windows from `bitbucket <https://bitbucket.org/SPLServices/seckit_sa_idm_windows/downloads/>`_
- See `installing apps <http://docs.splunk.com/Documentation/AddOns/released/Overview/Wheretoinstall>`_ This add on only requires installation on the search head in a distributed deployment.

Splunk Cloud:

- Using a service request ask for the app installation SecKit_SA_idm_windows id "3059" specify version 3.0 or later


Verify Installation
-------------------------------------------

*Verification*

- As an es_admin navigate to Splunk Enterprise Security
- Click the Search menu
- Click Search again
- Execute the search:

::

| inputlookup seckit_idm_windows_os_lookup

*verify results are returned.*

- Execute the search:

::

| inputlookup seckit_idm_windows_assets_lookup

*verify no error is report results may yield zero records.*


- Execute the search:

::

| inputlookup seckit_idm_windows_assets_identities_lookup

*verify no error is report results may yield zero records.*

- Execute the search:

::

| inputlookup seckit_idm_windows_identities_lookup

*verify no error is report results may yield zero records.*

Initialize Lookups and Collections
-------------------------------------------

- Navigate to a Splunk Search window

Run the following searches in order note the count of records may be zero continue following the installation documentation to complete the process.

Windows Identities
^^^^^^^^^^^^^^^^^^

**Build:**

::

	| from savedsearch:seckit_idm_windows_identities_lookup_gen

**Verification:**

::

| inputlookup seckit_idm_windows_identities_lookup

Windows Assets
^^^^^^^^^^^^^^

**Build:**

::

| from savedsearch:seckit_idm_windows_assets_lookup_gen

**Verification:**

::

| inputlookup seckit_idm_windows_assets_lookup

Windows Computer Identities
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Build:**

::

| from savedsearch:seckit_idm_windows_assets_identities_lookup_gen

**Verification:**

::

| inputlookup seckit_idm_windows_assets_identities_lookup

Force Enterprise Asset and Identity Merge
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Run the search:**

::

| from savedsearch:"Identity - Asset String Matches - Lookup Gen"

**Run the search:**

::

| from savedsearch:"Identity - Asset CIDR Matches - Lookup Gen"


**Run the search:**

::

| from savedsearch:"Identity - Identity Matches - Lookup Gen"

Continue to :ref:`data_activedirectory`
