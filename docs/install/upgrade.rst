.. SecKit SA IDM Common documentation master file, created by

.. _upgrade:

================================================
Upgrading from prior versions
================================================

Upgrade from version <2.0
-------------------------------------------

Changes from version 1.0 are drastic, recommendation is to remove the apps, and
review the contents of ``local/*.conf`` and lookups and port the config as if a
new installation.

Upgrade from version 2.x
-------------------------------------------

The macro get_manager has been renamed to seckit_idm_windows_get_identity_manager if this has been customized move the customizations to the new macro
