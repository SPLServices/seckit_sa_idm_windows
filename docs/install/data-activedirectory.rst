.. SecKit SA IDM Windows documentation master file, created by

.. _data_activedirectory:

================================================
Collect Active Directory Data
================================================

Collection of Active Directory data from all domains in all forests is required for the correct use of this app. In many instances the collection is already occurring but the records may not be retained as needed.

Determine if events have been collected
---------------------------------------

Execute the following search to determine if any previous collect has occurred:

.. code-block:: text

 index=* sourcetype=ActiveDirectory dNSHostName=*
 | rex field=dNSHostName "\.(?<domain>.*)"
 | stats values(index) dc(dNSHostName) as number_computers earliest(_time) as first_event latest(_time) as last_event dc(host) as Number_DCs by domain


If the number of computers is similar to the expected number of computers by domain and the number of DCs is <4 the existing data can be used. If no results are returned or the results are in question continue to collecting ActiveDirectory Events

Collecting Active Directory Events
---------------------------------------

Splunk ``sourcetype=ActiveDirectory`` events are generated when a object change or replication of an object change occurs. SecKit typicaly utilizes the index ``appmsadmon`` for these events if another index or multiple indexes will be used ensure the search macro ``seckit_idm_windows_adindex`` is properly updated to reference the indexes.

SecKit Windows contains prescriptive guidance for collection of Windows data available here `<https://seckit.readthedocs.io/projects/splunk-ta-windows-seckit/en/latest/index.html>`_ If the entire SecKit Windows guidance is not used ensure the following activities are performed.

- Implement the Universal Forwarder managed by a deployment server or alternative configuration management solution for all Active Directory Servers in all domains.
- Ensure WinEventLog:Security with ``renderxml=true`` is collected on all Active Directory servers using Splunk_TA_windows v5.0.0 or later
- Create a new index ``appsmsadmon`` or name selected based on internal standards exists on the indexer cluster configured for at least 90 days of storage.
- Using deployment server (or other config management solution) place the following config stanza on two selected Active Directory servers per domain. ** DO NOT DEPLOY TO ALL SERVERS ** Update the index as required and remove any other ``admon://`` stanzas from previous configuration.

.. code-block

::

 [admon://seckit]
 disabled = 0
 baseline = 1
 monitorSubtree = 1
 index=appmsadmon

Verify by repeating the steps to determine if events have been collected for all domains.

Load Object Collections
---------------------------------------

SecKit IDM uses collections to cache Active Directory objects for later use.

- Run the search:

::

|  from savedsearch:seckit_idm_windows_activedirectory_computers_load

This will build the initial cache of computer objects the number returned as a result should represent the number of computers in the domain.

- Run the search:

::

|  from avedsearch:seckit_idm_windows_activedirectory_persons_load

This will build the initial cache of user objects the number returned as a result should represent the number of users in the domain.

Build initial asset and identity lookups
----------------------------------------

If a new domain is on boarded, the normally scheduled searches will consolidate and merge the information within 24 hours. To expedite the process this can be safely repeated.

Run the following searches in order:

Windows Identities
^^^^^^^^^^^^^^^^^^

**Build:**

::

| from savedsearch:seckit_idm_windows_identities_lookup_gen

**Verification:**

::

| inputlookup seckit_idm_windows_identities_lookup

Windows Assets
^^^^^^^^^^^^^^

**Build :**

::

| from savedsearch:seckit_idm_windows_assets_lookup_gen

**Verification:**

::

| inputlookup seckit_idm_windows_assets_lookup
  
Windows Computer Identities
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Build:**

::

| from savedsearch:seckit_idm_windows_assets_identities_lookup_gen

**Verification:**

::

| inputlookup seckit_idm_windows_assets_identities_lookup

Force Enterprise Asset and Identity Merge
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Run the search:**

::

| from savedsearch:"Identity - Asset String Matches - Lookup Gen"

**Run the search:**

::

| from savedsearch:"Identity - Asset CIDR Matches - Lookup Gen"

**Run the search:**

::

| from savedsearch:"Identity - Identity Matches - Lookup Gen"
