.. SecKit SA IDM Common documentation master file, created by

.. _customizing:

================================================
Customizing the add on
================================================

The add on support customization using macros

Index Location Macros
--------------------------------------------------------------------------

- seckit_idm_windows_adindex The index(es) where sourcetype=ActiveDirectory can be found
- seckit_idm_windows_winhostmon_adindex The index(es) where sourcetype=WinHostMon can be found
- seckit_idm_windows_winscripts_adindex The index(es) where scripted inputs for the Windows OS can be found this is used to identify static IP addresses
- seckit_idm_windows_winevents_adindex The index(es) where Windows Security events can be located
- seckit_idm_windows_nixscripts_adindex The index(es) where scripted inputs for the Nix OS can be found this is used to identify static IP addresses for domain joined Nix assets

seckit_idm_windows_get_asset_bunit
--------------------------------------------------------------------------

This macro is utilized to customize the format of the bunit field for ES Assets it should set the bunit field.


seckit_idm_windows_get_asset_owner
--------------------------------------------------------------------------

This macro is utilized to customize the owner field for the asset this should set the value of owner

seckit_idm_windows_get_asset_priority
--------------------------------------------------------------------------

This macro allows customization of the priority field for the asset this should the field custom_priority

seckit_idm_windows_get_asset_category
--------------------------------------------------------------------------

This macro allows customization of the category for the asset it should set/build the value of the mvfield custom_category

seckit_idm_windows_get_identity_bunit
--------------------------------------------------------------------------

This macro is utilized to customize the format of the bunit field for ES Identity it should set the bunit field.

seckit_idm_windows_get_identity_priority
--------------------------------------------------------------------------
This macro is set used to set the identity priority is should set a field value of custom_priority

seckit_idm_windows_get_identity_manager
--------------------------------------------------------------------------

This macro is utilized to set the manager of the identity record generally this is in the form of the managers username or primary email. The field should set the field manager


seckit_idm_windows_get_asset_ip_custom
--------------------------------------------------------------------------
This macro is set used to set the identity priority is should set a field value of ip_custom which can be multivalue. Do not set the ip value for hosts which are DHCP managed.

seckit_idm_windows_get_asset_mac_custom
--------------------------------------------------------------------------
This macro is set used to set the asset MAC field is should set a field value of mac_custom which can be multivalue.
