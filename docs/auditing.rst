.. SecKit SA IDM Common documentation master file, created by

.. _auditing:

================================================
Auditing
================================================

Scheduling and KVstore audit
============================

**The SecKit SA IDM for Windows assets provides out of the box an audit dashboard for scheduling activity and KVstore collections:**

- **SecKit for Windows - Audit Scheduling and KVstore collections**

**This dashboard is available in any application and provides key statistics and visualizations on top of:**

- Main aggregated statistics for scheduling: average number of scheduled searches, run time statistics, etc
- Main aggregated statistics for KVstore collections: number of collections, cumulated size of collections and accelerations, etc
- Over time visualization of scheduled reports activity
- Most time expensive scheduled reports, frequency
- Details table of scheduled reports with statistics and configuration
- Details table of KVstore collections hosted in the application

.. image:: _static/audit_scheduling_and_kv_screen1.png
   :alt: audit_scheduling_and_kv_screen1.png
   :align: center
