.. SecKit SA IDM Common documentation master file, created by

.. _categories:

================================================
Windows Categories
================================================

The following categories are commonly defined in the categories configuration. The shortest reasonable string should be used for all values. Note only values matching the regex [A-Za-z0-9\-_] should be used.

pf:<value> BOTH
+++++++++++++++++++++++++++++++++++

The PF or primary function of a device is a specific identifier relates to the role of a asset in a service. This is most commonly applied to a specific asset but may apply to a CIDR

svc:<value> BOTH
+++++++++++++++++++++++++++++++++++

The SVC or Service is a identifier indicating the service this asset participates in providing for example. The service DNS "svc:DNS" would typlically be made up of a combination of "pf:ms_dns" or "pf:BIND" AND "pf:dns_rbl" "pf:dns_recursive"
