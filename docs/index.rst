.. SecKit SA IDM Windows documentation master file, created by


==================================================
Welcome to SecKit SA IDM Windows's documentation!
==================================================

Version: |version|

Release: |release|

Success Enablement Content "SecKit" apps for Splunk are designed
to accelerate the tedious or difficult tasks. This application IDM Windows
is an add on for Splunk Enterprise Security designed to identify and enrich
asset and identity information based on Microsoft Active Directory. Assets and Identities
based on Active Directory can give critical insights into machine data

- Is this device properly domain joined?
- Who is responsible for this device?
- Does this user have a privileged account?

Before you get started
======================

- Implement v3.0.0 or newer of `SecKit IDM Windows Common <https://seckit-sa-idm-windows.readthedocs.io/en/develop/categories.html>`_.
- Complete Splunk Enterprise Security Administration training
- Review the current Assets and Identities section of the `Administration Manual <http://docs.splunk.com/Documentation/ES/latest/Admin/Addassetandidentitydata>`_
- Review the use of lookup data in Splunk
   - `Lookup Command <https://docs.splunk.com/Documentation/Splunk/latest/Knowledge/LookupexampleinSplunkWeb>`_
   - `CIDR and Matching Rules <http://docs.splunk.com/Documentation/Splunk/7.1.3/Knowledge/Addfieldmatchingrulestoyourlookupconfiguration>`_

Support
======================


- Reporting issues or requesting enhancements `Issue Tracker <https://bitbucket.org/SPLServices/seckit_sa_idm_common/issues?status=new&status=open>`_
- `Source <https://bitbucket.org/SPLServices/seckit_sa_idm_windows>`_


Documentation
======================

.. toctree::
   :maxdepth: 2
   :glob:

   requirements
   install/install
   install/upgrade
   install/data-activedirectory
   quickstart
   using
   customizing
   categories
   auditing
