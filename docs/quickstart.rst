.. SecKit SA IDM Common documentation master file, created by

.. _quickstart:

================================================
Quick Start Tutorial
================================================

The quick start procedure is simply to demonstrate the application of this solution continue
reading in the using guide once your first use is complete. If you have not already done so follow
the install and data-activedirectory portions of the documentation first.

Identifying important assets and identities
-------------------------------------------

- Working with a knowledgeable identify a specific Windows server (or servers) that by name or name pattern can be identified as high or critical. For example the file server used by the CEO.
- Working with a knowledgeable Active Directory administrator identify one group with current members that grants privileged access to Active Directory or Member Servers OTHER than the default groups created by Active Directory such as Enterprise Admins, Domain Admins or Administrators


Configure categorization for a single server
--------------------------------------------

- Login to Enterprise Security
- Navigate to Enterprise security
- Select Configure menu
- Select Content management
- Select "SecKit SA IDM Common for ES" in the app drop down
- Find "SecKit IDM Common static hosts"
- Under actions for this row select export
- Using a csv editor of your choice Add the following information and save. wild card values for static name are allowed as are specific host names. Wild cards should use should be limited to the end of the host name to avoid accidental match to unintended hosts.

+--------------+----------------------+-------------------+-------------------+-------------------+
| static_name  | static_category      | static_pci_domain | static_priority   | static_expected   |
+--------------+----------------------+-------------------+-------------------+-------------------+
| srvexevfs*   | estaff               | trust             | high              | true              |
+--------------+----------------------+-------------------+-------------------+-------------------+

- return to Enterprise Security
- Under Actions for this row click Update File
- Select the modified file

**Run the following searches:**

**Windows Assets:**

Run the search:

::

| savedsearch seckit_idm_windows_identities_lookup_gen

**Force Enterprise Identity Merge:**

- Run the search:

::

| from savedsearch:"Identity - Asset Identity Matches - Lookup Gen"

Verify categorization for a single server
-----------------------------------------

- Return to Enterprise Security
- Select the Security Domains menu
- Select Identity
- Select Asset Center
- Enter a specific host matching static name above
- Verify the category, pci_domain and priority fields match above


Configure categorization for privileged group
---------------------------------------------

- Login to Enterprise Security
- Navigate to Enterprise security
- Select Configure menu
- Select Content management
- Select "SecKit SA IDM Windows for ES" in the app drop down
- Find "SecKit IDM Windows AD Identity Classification By memberOf Group"
- Under actions for this row select export
- Using a csv editor of your choice Add the following information and save. wild card values for static name are allowed as are specific host names. Wild cards should use should be limited to the end of the host name to avoid accidental match to unintended hosts.

+-------------------------------------------------+----------------------+-------------------+-------------------+
| memberOf                                        | member_category      | member_priority   | member_watchlist  |
+-------------------------------------------------+----------------------+-------------------+-------------------+
| CN=System Managed Accounts Group,CN=Builtin,*   | nha                  | critical          | high              |
+-------------------------------------------------+----------------------+-------------------+-------------------+

- return to Enterprise Security
- Under Actions for this row click Update File
- Select the modified file

**Run the following searches:**

**Windows Identities**

Build: 

::

| savedsearch seckit_idm_windows_assets_lookup_gen

**Force Enterprise Asset and Identity Merge:**

- Run the search:

::

| from savedsearch:"Identity - Asset String Matches - Lookup Gen"

- Run the search:

::

| from savedsearch:"Identity - Asset CIDR Matches - Lookup Gen"

Verify categorization for privileged group
------------------------------------------

- Return to Enterprise Security
- Select the Security Domains menu
- Select Identity
- Select Identity Center
- Enter a specific user included in the group
- Verify the category and priority fields match above
