# Security Kit #

##Identity Management Windows Components##
 SecKit_SA_idm_windows

### Description ###

This purpose of this Splunk add on is to provide foundational tools and routines for the population of assets and identities in the Enterprise Security and PCI applications for Splunk. Using this add-on develop a solid base declaration of assets with prioritization and categorization for your network.


### Installation ###

[Package Readme](src/SecKit_SA_idm_windows/README.md)

## Contributing ##

PRs require copyright assignment for contributors not employed by Splunk Inc.

## Support ##

Direct contact Community Support as best effort Ryan Faircloth rfarcloth@splunk.com

[Issue Tracker](https://bitbucket.org/SPLServices/seckit_sa_idm_windows/issues?status=new&status=open)

### Source ###

[bitbucket](https://bitbucket.org/SPLServices/seckit_sa_idm_windows)

### Blog ###

Follow me and seek more knowledge at <http://www.rfaircloth.com/2016/01/07/making-asset-data-useful-with-splunk-enterprise-security/>
