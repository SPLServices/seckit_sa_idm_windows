#
# Seckit Identity Management Windows AD for ES  - A utility add on to properly format Assets and
# Identifies for ES using Windows AD information
#
# Copyright 2017-2018 Splunk Inc, <rfaircloth@splunk.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
[lookup:seckit_idm_windows_activedirectory_computers_lookup]
description = Maintain a lookup computers to include in ES assets andd identities
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_windows_activedirectory_computers_lookup
label = SecKit IDM Windows Active Directory Computer Object Tracker
lookup_type = search
savedsearch = seckit_idm_windows_activedirectory_computers_tracker

[lookup:seckit_idm_windows_activedirectory_persons_lookup]
description = Maintain a lookup persons to include in ES identities
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_windows_activedirectory_persons_lookup
label = SecKit IDM Windows Active Directory Person Object Tracker
lookup_type = search
savedsearch = seckit_idm_windows_activedirectory_persons_tracker

[lookup:seckit_idm_windows_assets_identities_lookup]
description = List of all computer accounts in Active Directory for ES
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_windows_assets_identities_lookup
label = SecKit IDM Windows Identities Computer Accounts
lookup_type = search
savedsearch = seckit_idm_windows_assets_identities_lookup_gen

[lookup:seckit_idm_windows_assets_lookup]
description = List of all assets in Active Directory for ES
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_windows_assets_lookup
label = SecKit IDM Windows assets for ES
lookup_type = search
savedsearch = seckit_idm_windows_assets_lookup_gen

[lookup:seckit_idm_windows_assets_bunit_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_assets_bunit_lookup
label       = SecKit IDM Windows AD Asset Classification By bunit
description = SecKit IDM Windows AD Asset Classification By bunit
lookup_type = adhoc

[lookup:seckit_idm_windows_assets_org_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_assets_org_lookup
label       = SecKit IDM Windows AD Asset Classification By Organizational Unit
description = SecKit IDM Windows AD Asset Classification By Organizational Unit
lookup_type = adhoc

[lookup:seckit_idm_windows_os_win_interfaces_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_os_win_interfaces_lookup
editable = 0
label       = SecKit IDM Windows OS interface type enrichment
description = Wildcard lookup by OS interface used to determine if an interface can be treated as static
lookup_type = adhoc

[lookup:seckit_idm_windows_os_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_os_lookup
editable = 0
label       = SecKit IDM Windows AD Asset Classification By OS
description = SecKit IDM Windows AD Asset Classification By OS
lookup_type = adhoc

[lookup:seckit_idm_windows_pf_roles_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_pf_roles_lookup
label       = SecKit IDM Windows AD Asset Classification By OS Installed Role
description = SecKit IDM Windows AD Asset Classification By OS Installed Role
lookup_type = adhoc
editable = 0

[lookup:seckit_idm_windows_asset_mac_lookup]
description = Maintain a lookup for MAC to asset
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_windows_asset_mac_lookup
label = SecKit IDM Windows Addon MAC to Asset
lookup_type = search
savedsearch = seckit_idm_windows_asset_mac_tracker

[lookup:seckit_idm_windows_asset_os_role_lookup]
description = Maintain a lookup for os role to asset
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_windows_asset_os_role_lookup
label = SecKit IDM Windows Addon os role to Asset
lookup_type = search
savedsearch = seckit_idm_windows_asset_os_role_tracker

[lookup:seckit_idm_windows_identities_lookup]
description = List of all accounts in Active Directory for ES
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_windows_identities_lookup
label = SecKit IDM Windows identities for ES
lookup_type = search
savedsearch = seckit_idm_windows_identities_lookup_gen

[lookup:seckit_idm_windows_identities_accounts_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_identities_accounts_lookup
label       = SecKit IDM Windows AD Identity Classification By Account Name
description = SecKit IDM Windows AD Identity Classification By Account Name
lookup_type = adhoc

[lookup:seckit_idm_windows_identities_members_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_identities_members_lookup
label       = SecKit IDM Windows AD Identity Classification By memberOf Group
description = SecKit IDM Windows AD Identity Classification By memberOf Group
lookup_type = adhoc

[lookup:seckit_idm_windows_identities_bunit_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_identities_bunit_lookup
label       = SecKit IDM Windows AD Identity Classification By Bunit
description = SecKit IDM Windows AD Identity Classification By Bunit
lookup_type = adhoc

[lookup:seckit_idm_windows_identities_nha_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_identities_nha_lookup
label       = SecKit IDM Windows AD NHA Identity Classification By Account Name
description = SecKit IDM Windows AD NHA Identity Classification By Account Name
lookup_type = adhoc

[lookup:seckit_idm_windows_identities_org_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_identities_org_lookup
label       = SecKit IDM Windows AD Identity Classification By Organizational Unit
description = SecKit IDM Windows AD Identity Classification By Organizational Unit
lookup_type = adhoc

[lookup:seckit_idm_windows_identities_title_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_windows_identities_title_lookup
label       = SecKit IDM Windows AD Identity Classification By Title
description = SecKit IDM Windows AD Identity Classification By Title
lookup_type = adhoc
