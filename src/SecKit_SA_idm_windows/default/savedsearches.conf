#
# Seckit Identity Management Windows AD for ES  - A utility add on to properly format Assets and
# Identifies for ES using Windows AD information
#
# Copyright 2017-2018 Splunk Inc, <rfaircloth@splunk.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

[seckit_idm_windows_assets_identities_lookup_gen]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = 0 */4 * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
search = | `seckit_idm_windows_ad_assets_identities`

[seckit_idm_windows_assets_lookup_gen]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = 0 */4 * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_common
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
search = | `seckit_idm_windows_ad_assets`

[seckit_idm_windows_identities_lookup_gen]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = 0 */4 * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_common
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
search = | `seckit_idm_windows_ad_identities`

[seckit_idm_windows_pf_roles_lookup_gen]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = 0 * * * *
dispatch.earliest_time = -1h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = false
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
search = |inputlookup seckit_idm_windows_pf_roles_lookup\
| append [search \
`seckit_idm_windows_winhostmon_adindex` earliest=-7d sourcetype=winhostmon source=roles | rename Name as role_name | dedup role_name]\
| dedup role_name  \
| fillnull value="" role_category role_priority \
| lookup  seckit_idm_windows_pf_roles_lookup role_name OUTPUT role_category role_priority \
| fields + role_name role_category role_priority\
| outputlookup  seckit_idm_windows_asset_os_role_lookup createinapp=true create_empty=false append=false\
| stats count

[seckit_idm_windows_activedirectory_computers_load]
dispatch.earliest_time = -2y
dispatch.latest_time = -5m@m
dispatch.ttl = 10
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
search = earliest=-2y `seckit_idm_windows_activedirectory_computers`

[seckit_idm_windows_activedirectory_computers_tracker]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = */5 * * * *
dispatch.earliest_time= -10m@m
dispatch.latest_time = -5m@m
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 0
search = `seckit_idm_windows_activedirectory_computers`

[seckit_idm_windows_activedirectory_persons_load]
dispatch.earliest_time= -7y
dispatch.latest_time = -5m@m
dispatch.ttl = 10
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
search = earliest=-2y  `seckit_idm_windows_activedirectory_persons`

[seckit_idm_windows_activedirectory_persons_tracker]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = */5 * * * *
dispatch.earliest_time= -10m@m
dispatch.latest_time = -5m@m
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 0
search = `seckit_idm_windows_activedirectory_persons`

[seckit_idm_windows_asset_interface_load]
dispatch.earliest_time= -7d
dispatch.latest_time = -5m@m
dispatch.ttl = 10
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
search = `seckit_idm_windows_asset_interface`

[seckit_idm_windows_asset_interface_tracker]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = */5 * * * *
dispatch.earliest_time= -10m@m
dispatch.latest_time = -5m@m
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
search = `seckit_idm_windows_asset_interface`

[seckit_idm_windows_asset_os_role_load]
dispatch.earliest_time= -7d
dispatch.latest_time = -5m@m
dispatch.ttl = 10
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
search = `seckit_idm_windows_asset_interface`

[seckit_idm_windows_asset_os_role_tracker]
action.customsearchbuilder.routine = make_lookup_generating_search:makeLookupGeneratingSearch
action.customsearchbuilder.spec = {}
cron_schedule = */30 * * * *
dispatch.earliest_time= -35m@m
dispatch.latest_time  = -5m@m
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
search = `seckit_idm_windows_asset_os_role`

[seckit_idm_windows_assets_bunit_lookup_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | makeresults \
| eval bunit="unmatchablehost" \
| inputlookup append=t seckit_idm_windows_assets_bunit_lookup \
| dedup bunit \
| fields + bunit,bunit_category,bunit_priority,bunit_expected\
| outputlookup create_empty=true createinapp=true  seckit_idm_windows_assets_bunit_lookup \
| stats count

[seckit_idm_windows_assets_org_lookup_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | inputlookup seckit_idm_windows_assets_org_default_lookup\
| inputlookup append=t seckit_idm_windows_assets_org_lookup\
| dedup org\
| fields + org,org_category,org_priority,org_expected\
| outputlookup seckit_idm_windows_assets_org_lookup\
| stats count

[seckit_idm_windows_identities_bunit_lookup_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | makeresults \
| eval bunit="unmatchablehost" \
| inputlookup append=t seckit_idm_windows_identities_bunit_lookup \
| dedup bunit \
| fields + bunit,bunit_category,bunit_priority,bunit_watchlist,bunit_expected\
| outputlookup create_empty=true createinapp=true  seckit_idm_windows_identities_bunit_lookup \
| stats count

[seckit_idm_windows_identities_org_lookup_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | makeresults \
| eval org="unmatchable" \
| inputlookup append=t seckit_idm_windows_identities_org_lookup \
| dedup org \
| fields + org,org_category,org_priority,org_watchlist,org_expected\
| outputlookup create_empty=true createinapp=true  seckit_idm_windows_identities_org_lookup \
| stats count

[seckit_idm_windows_identities_title_lookup_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | makeresults \
| eval title="unmatchable" \
| inputlookup append=t seckit_idm_windows_identities_title_lookup \
| dedup title \
| fields + title,title_category,title_priority,title_watchlist\
| outputlookup create_empty=true createinapp=true  seckit_idm_windows_identities_title_lookup \
| stats count

[seckit_idm_windows_identities_accounts_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | inputlookup seckit_idm_windows_identities_accounts_lookup\
| inputlookup append=t seckit_idm_windows_identities_accounts_default_lookup\
| dedup account\
| fields + account,account_category,account_priority,account_watchlist\
| outputlookup seckit_idm_windows_identities_accounts_lookup\
| stats count

[seckit_idm_windows_identities_members_lookup_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | inputlookup seckit_idm_windows_identities_members_lookup\
| inputlookup append=t seckit_idm_windows_identities_members_default_lookup\
| dedup memberOf\
| fields + memberOf,member_category,member_priority,member_watchlist\
| outputlookup seckit_idm_windows_identities_members_lookup\
| stats count


[seckit_idm_windows_identities_nha_lookup_ftr]
cron_schedule = */5 * * * *
dispatch.earliest_time = -2h
dispatch.latest_time = now
dispatch.ttl = 10
enableSched = true
request.ui_dispatch_app = SecKit_SA_idm_windows
request.ui_dispatch_view = search
schedule_window = auto
allow_skew = 100%
realtime_schedule = 1
run_on_startup = true
run_n_times = 1
is_visible = false
search = | inputlookup seckit_idm_windows_identities_nha_lookup\
| inputlookup append=t seckit_idm_windows_identities_nha_default_lookup\
| dedup identity\
| fields + identity,nha_category,nha_watchlist,nha_priority\
| outputlookup seckit_idm_windows_identities_nha_lookup\
| stats count
