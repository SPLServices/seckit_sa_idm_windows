#
# Seckit Identity Management Windows AD for ES  - A utility add on to properly format Assets and
# Identifies for ES using Windows AD information
#
# Copyright 2017-2018 Splunk Inc, <rfaircloth@splunk.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
[seckit_idm_windows_assets_bunit_lookup]
filename = seckit_idm_windows_assets_bunit.csv
fields_list = bunit,bunit_category,bunit_priority,bunit_expected
match_type = WILDCARD(bunit)
case_sensitive_match = false

[seckit_idm_windows_assets_identities_lookup]
filename = seckit_idm_windows_assets_identities.csv
fields_list = identity,prefix,nick,first,last,suffix,email,phone,phone2,managedBy,priority,bunit,category,watchlist,startDate,endDate,work_city,work_country,work_lat,work_long

[seckit_idm_windows_assets_org_default_lookup]
filename = seckit_idm_windows_assets_org_default.csv
fields_list = org,org_category,org_priority,org_expected
match_type = WILDCARD(org)
case_sensitive_match = false

[seckit_idm_windows_assets_org_lookup]
filename = seckit_idm_windows_assets_org.csv
fields_list = org,org_category,org_priority,org_expected
match_type = WILDCARD(org)
case_sensitive_match = false

[seckit_idm_windows_assets_lookup]
filename = seckit_idm_windows_assets.csv
fields_list = ip,mac,nt_host,dns,owner,priority,lat,long,city,country,bunit,category,pci_domain,is_expected,should_timesync,should_update,requires_av

[seckit_idm_windows_identities_accounts_default_lookup]
filename = seckit_idm_windows_identities_accounts_default.csv
fields_list = account,account_category,account_priority,account_watchlist
match_type = WILDCARD(account)
case_sensitive_match = false

[seckit_idm_windows_identities_accounts_lookup]
filename = seckit_idm_windows_identities_accounts.csv
fields_list = account,account_category,account_priority,account_watchlist
match_type = WILDCARD(account)
case_sensitive_match = false

[seckit_idm_windows_identities_bunit_lookup]
filename = seckit_idm_windows_identities_bunit.csv
fields_list = bunit,bunit_category,bunit_watchlist,bunit_priority
match_type = WILDCARD(bunit)
case_sensitive_match = false

[seckit_idm_windows_identities_members_default_lookup]
filename = seckit_idm_windows_identities_members_default.csv
fields_list = memberOf,member_category,member_priority,member_watchlist
match_type = WILDCARD(memberOf)
case_sensitive_match = false

[seckit_idm_windows_identities_members_lookup]
filename = seckit_idm_windows_identities_members.csv
fields_list = memberOf,member_category,member_priority,member_watchlist
match_type = WILDCARD(memberOf)
case_sensitive_match = false

[seckit_idm_windows_identities_nha_default_lookup]
filename = seckit_idm_windows_identities_nha_default.csv
fields_list = identity,nha_category,nha_watchlist,nha_priority
case_sensitive_match = false

[seckit_idm_windows_identities_nha_lookup]
filename = seckit_idm_windows_identities_nha.csv
fields_list = identity,nha_category,nha_watchlist,nha_priority
case_sensitive_match = false

[seckit_idm_windows_identities_org_lookup]
filename = seckit_idm_windows_identities_org.csv
fields_list = org,org_category,org_watchlist,org_priority
match_type = WILDCARD(org)
case_sensitive_match = false

[seckit_idm_windows_identities_title_lookup]
filename = seckit_idm_windows_identities_title.csv
fields_list = title,title_category,title_priority,title_watchlist
match_type = WILDCARD(title)
case_sensitive_match = false

[seckit_idm_windows_identities_lookup]
filename = seckit_idm_windows_identities.csv
fields_list = identity,prefix,nick,first,last,suffix,email,phone,phone2,managedBy,priority,bunit,category,watchlist,startDate,endDate,work_city,work_country,work_lat,work_long

[seckit_idm_windows_os_win_interfaces_lookup]
filename = seckit_idm_windows_os_win_interfaces.csv
fields_list = InterfaceDescription,ExternalStatic,Private
case_sensitive_match = false
match_type = WILDCARD(InterfaceDescription)

[seckit_idm_windows_os_lookup]
filename = seckit_idm_windows_os.csv
fields_list = operatingSystem,os_vendor,os_class,os_product,os_version,os_supported,requires_av
case_sensitive_match = false
match_type = WILDCARD(operatingSystem)

[seckit_idm_windows_pf_roles_lookup]
filename = seckit_idm_windows_pf_roles.csv
fields_list = role_name,role_category,role_priority,role_expected

[seckit_idm_windows_activedirectory_computers_lookup]
external_type = kvstore
collection = seckit_idm_windows_activedirectory_computers
max_matches = 1
case_sensitive_match = false
allow_caching = true
fields_list = dNSHostName,managedBy,objectClass,distinguishedName,sAMAccountName,cn,userAccountControl,isDisabled,isDeleted,whenDeleted,whenCreated,whenChanged,operatingSystem,company,division,department,"_time",locator

[seckit_idm_windows_activedirectory_persons_lookup]
external_type = kvstore
collection = seckit_idm_windows_activedirectory_persons
max_matches = 1
case_sensitive_match = false
allow_caching = true
fields_list = first,last,suffix,email,phone,phone2,managedBy,company,division,department,bunit,watchlist,startDate,endDate,work_city,work_country,distinguishedName,objectCategory,name,sAMAccountName,employeeType,employeeID,displayName,memberOf,uid,uidNumber,title,objectSid,userPrincipalName,isDeleted,locator

[seckit_idm_windows_asset_interface_lookup]
external_type = kvstore
collection = seckit_idm_windows_asset_interface
case_sensitive_match = false
allow_caching = true
fields_list = _time,host,DHCPServers,Domain,IPv4Address,IPv4AddressState,IPv4Connectivity,IPv4DNS,IPv4PolicyStore,IPv4PreferredLifetime,IPv4PrefixLength,IPv4PrefixOrigin,IPv4SkipAsSource,IPv4SuffixOrigin,InterfaceAlias,InterfaceDescription,InterfaceIndex,LinkSpeed,MacAddress,NetworkCategoryhost

[seckit_idm_windows_asset_os_role_lookup]
external_type = kvstore
collection = seckit_idm_windows_asset_os_role
case_sensitive_match = false
allow_caching = true
fields_list = _time,host,role_name,role_category,role_priority,role_expected
